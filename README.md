# README #

## This solution will provide the following in Azure:
* App Service Plan (Basic so the Azure Function can store up to 10gb of temporary data)
* Keyvault
* Azure Function (Containing 3 different functions, cleanup, start backup, save backup)
* Storage account (Storage account v2 with cold storage)

## What is this repository for? ##

* A full solution that will deploy all the resources needed to automate Atlassian backups using Azure resources.
* Version 1.0

## How do I get set up? ##

* Run the deploy-azurefunction.ps1 script

## This script has a few parameters;

### SubscriptionId
This is the ID of the subscription you would like to deploy to.
### ResourceGroupName - Default value: "rg-atlassianbackup-prod-we"
Resource group name
### Location - Default value: 'West Europe'
How many copies of Confluence should be saved
### Environment - Default value: Prod
What environment are we deploying to (if you have multiple atlassian environments)
### RunningAsUser
This is the user in whos context we are running as, it is needed to give temporary access to the Azure KeyVault.
### AtlassianUser
Identity of the user who will create the needed API token at https://id.atlassian.com/manage-profile/security
### AtlassianAccount
Identifier of the atlassian account, <identifier>.atlassian.net
### JiraBackupCount - Default value: 7
How many copies of Jira should be saved
### ConfluenceBackupCount - Default value: 7
How many copies of Confluence should be saved
### Environment - Default value: 'prod'
How many copies of Confluence should be saved
### FunctionAppName - Default value: "backup"
Function application name

## Configure what products to backup
In the start-AtlassianCloudBackup you can in the run.ps1 edit the variables of $JiraBackup and $ConfluenceBackup to choose what products to backup (boolean)
If changed, please also change in the Save-AtlassianCloudBackup run.ps1 script or it will attempt to download the latest backup and save it.
## Dependencies
Az PowerShell module
Optional: Bicep (https://github.com/Azure/bicep)
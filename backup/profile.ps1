if ($env:MSI_SECRET -and (Get-Module -ListAvailable Az.Accounts)) {
    Connect-AzAccount -Identity
    Select-AzSubscription -Subscription 'placeholder'
}
    
$env:JiraBackupCount = 'placeholder'
$env:ConfluenceBackupCount = 'placeholder'

# Atlassian subdomain i.e. whateverproceeds.atlassian.net
$env:account = 'placeholder'
# username with domain something@domain.com
$env:username = 'placeholder'
$env:StorageAccount = 'placeholder'
$env:ResourceGroupName = 'placeholder'
$env:AzureFileShare = 'placeholder'
# Token created from product https://confluence.atlassian.com/cloud/api-tokens-938839638.html
# NOTE: The token retrieved from the user is bound to the user, if users is disabled then the token is disabled!
# NOTE: The user needs to be an administrator in the product to be able to perform backups; https://getsupport.atlassian.com/servicedesk/customer/portal/42/PSCLOUD-44535
    
